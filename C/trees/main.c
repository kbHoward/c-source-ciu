#include <stdlib.h>
#include <stdio.h>

typedef struct t_node t_node;
struct t_node{
    int value;
    t_node *left;
    t_node *right;
};

t_node* create_node(const int value);
int insert_node(t_node *root, const int value); // Insert value into a tree.
int get_node_count(const t_node *root); // get the count of values that are stored.
void print_values(const t_node *root); // prints the values in the tree, from min to max.
void delete_tree(t_node *root); // Remove the tree from memory.
int is_in_tree(const t_node *root, const int value); // Return true if given value exists in the tree.
int get_height(const t_node *root); // returns the height in nodes (single node's height is 1).
int get_min(const t_node *root); // Returns the minimum value stored in the tree.
int get_max(const t_node *root); // Returns the maximum value stored in the tree.
int is_binary_search_tree(const t_node *root); // ??
int get_successor(t_node *root, const int value); // Returns next-highest value
t_node* find_node(t_node *root, const int value);


int main(){
    
    t_node *root = create_node(5);

    printf("Insert status: %d for %d\n", insert_node(root, 10), 10);
    printf("Insert status: %d for %d\n", insert_node(root, 4), 4);
    printf("Insert status: %d for %d\n", insert_node(root, 7), 7);
    printf("Insert status: %d for %d\n", insert_node(root, 7), 7);
    printf("Insert status: %d for %d\n", insert_node(root, 2), 2);
    printf("Insert status: %d for %d\n", insert_node(root, 5823), 5823);
    printf("Insert status: %d for %d\n", insert_node(root, 1), 1);
    printf("Insert status: %d for %d\n", insert_node(root, 9), 9);
    printf("Insert status: %d for %d\n", insert_node(root, 3), 3);
    printf("Insert status: %d for %d\n", insert_node(root, 99), 99);
    
    printf("\n\n");
    print_values(root);
    printf("\n\n");
    const int i = 10;
    t_node* n = find_node(root, i);

    if (n != NULL){
        printf("root value: %d ", n->value);
        if (n->left != NULL) printf("left: %d ", n->left->value);
        if (n->right != NULL) printf("right: %d ", n->right->value);
        printf("\n");
    } 
    
    printf("Found %d in tree.\n", is_in_tree(root, 99));
    printf("Found %d in tree.\n", is_in_tree(root, 9));
    printf("Found %d in tree.\n", is_in_tree(root, 5));
    printf("Found %d in tree.\n", is_in_tree(root, 899));
    printf("Min %d in tree.\n", get_min(root));
    printf("Max %d in tree.\n", get_max(root));
    printf("Successor of %d -> %d in tree.\n", 5823, get_successor(root, 5823));

    delete_tree(root);

    return 0;
}

/* 
 * Create and return a new node.
 */
t_node *create_node(const int value){
    t_node *node = (t_node *) malloc(sizeof(t_node));
    node->value = value;
    node->left = NULL;
    node->right = NULL;

    return node;
}

/* 
 * Insert a new node into the binary tree 
 * depending on its value.
 */
int insert_node(t_node *root, const int value){

    /* Go Right */
    if (value > root->value) {
        if (root->right == NULL){
            root->right = create_node(value); 
            return 1;
        }
        else {
            insert_node(root->right, value);
        }
    }
    /* Go Left */
    else if (value < root->value) {
        if (root->left == NULL ) {
            root->left = create_node(value); 
            return 1;
        }
        else {
            insert_node(root->left, value);
        }
    }
    if (value == root->value)
        return 0;

    return 0;
}

/* 
 * Print the values in a binary tree.
 */
void print_values(const t_node *root){
    const t_node *ptr = root;
    printf("%d\n",ptr->value);
    if (ptr->left != NULL){
        print_values(ptr->left);
    }
    if (ptr->right != NULL){
        print_values(ptr->right);
    }
    
    ptr = NULL;
}

/* 
 * Find value in tree root.
 */
int is_in_tree(const t_node *root, const int value){
    if (value > root->value && root->right != NULL)
        return is_in_tree(root->right, value);
    if (value < root->value && root->left!= NULL)
        return is_in_tree(root->left, value);
    if (root->value == value) 
        return 1;
    else 
        return 0;
}

/*
 * Find miniumum value in tree.
 */
int get_min(const t_node *root){
    const t_node *ptr = root;
    while (ptr->left != NULL)
        ptr = ptr->left;

    return ptr->value;
}

/*
 * Find Maximum value in a tree.
 */
int get_max(const t_node *root){
    const t_node *ptr = root;
    while (ptr->right != NULL)
        ptr = ptr->right;
     
    return ptr->value;
}

/* 
 * Return a node with the value value.
   else return NULL.
 */
t_node* find_node(t_node *root, const int value){
    if (value > root->value && root->right != NULL){
        return find_node(root->right, value);
    }

    else if (value < root->value && root->left != NULL){
        return find_node(root->left, value);
    }
    else if (root->right == NULL && root->left == NULL)
        return NULL;
    if (root->value == value) return root;

    return NULL;
}

/*
 * Get successor return next highest value in 
 * tree after given value, -1 if none.
 */
int get_successor(t_node *root, const int value){
    t_node *ptr = find_node(root, value);
    int v = 0;
    if (ptr->right != NULL){
        t_node *l = ptr->right;
        while (l != NULL){
            if (l->left == NULL) return l->value;
            else{
                l = l->left;
                v = l->value;
            }
        }
            return v;
    }
    else 
        return ptr->value;

}

void delete_tree(t_node *root){
    if (root != NULL){ 
        if (root->left != NULL)    
            delete_tree(root->left);
        if (root->right != NULL)
            delete_tree(root->right);

    }
    free(root);
}
