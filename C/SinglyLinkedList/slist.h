#ifndef SLIST_H
#define SLIST_H

#include <stdlib.h>

typedef struct list_node list_node;
struct list_node {
    int value;
    list_node *next;
};

typedef struct slist slist;
struct slist{
    list_node *head;
    list_node *tail;
    size_t size;
};


/* --------- Node Functions ----------- */
list_node* create_node(const int value);
int free_node(list_node *n);

/* --------- SList Functions ----------- */
slist* allocate();
int is_empty(slist *lst);
size_t size(slist *lst);
int value_at(slist *lst, const int index);
list_node* node_at(slist *lst, const int index);
void push_front(slist *lst, list_node *n);
void push_back(slist *lst, list_node *n);
int pop_front(slist *lst);
int pop_back(slist *lst);
int free_list(slist *lst);
int front(const slist *lst);
int back (const slist *lst);
int insert(slist *lst, list_node *n, const size_t index);
int erase(slist *lst, const size_t index);
int value_n_from_end(const slist *lst, const size_t index);
void reverse(slist *lst);
void remove_value(slist *lst, const int v);
#endif
