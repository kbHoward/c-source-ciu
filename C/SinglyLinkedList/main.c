#include <stdlib.h>
#include <stdio.h>

#include "slist.h"

int main() {

    slist *l = allocate();
    list_node *n = (list_node*) create_node(8);
    list_node *n2 = (list_node*) create_node(9);
    list_node *n3 = (list_node*) create_node(10);
    list_node *n4 = (list_node*) create_node(50);
    list_node *n6 = (list_node*) create_node(26);
    list_node *n7 = (list_node*) create_node(27);

    push_front(l, n);
    push_front(l, n2);
    push_front(l, n3);
    push_front(l, n4);

    l->tail = n;
    l->tail->next = NULL;

    list_node *n5 = (list_node*) create_node(999);
    push_back(l, n6);
    push_back(l, n7);

    printf("insert at %d : %d\n", 2, insert(l,n5, 2));
    
    printf("erase at %d: %d\n", 2, erase(l, 2));
    printf("erase at %d: %d\n", 2, erase(l, 1));


//    printf("pop back:%d \n", pop_back(l));
 //   l->tail = node_at(l, l->size-1);
  //  l->tail->next = NULL;
    
    list_node *n8 = (list_node*) create_node(9999);
    list_node *n9 = (list_node*) create_node(200000);

    push_front(l, n8);
    push_front(l, n9);

    reverse(l);
    list_node *itr = l->head;

    remove_value(l, 9999);
    
    size_t c = 0;
    while (itr) {
        printf("node value %lu: %d\n", c, itr->value);
        itr = itr->next;
        c++;
    }

    printf("\n\n\n");

    printf("value at head: %d\n", front(l));
    printf("value at back: %d\n", back(l));
    printf("value at %d from end: %d\n", 2, value_n_from_end(l, 2));
    printf("size of list: %lu\n", size(l));

    free(itr);
    if (itr) itr = NULL;

    printf("Free status: %d\n", free_list(l));

    return 0;
}
