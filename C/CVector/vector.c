#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vector.h"

/* 
    Allocate vector v on heap,
    allocate vector->data
     return a pointer to vector v.
*/
vec* vinit(const size_t cap) {

    size_t alloc_size = VSIZE;
    while (alloc_size < cap)
        alloc_size *= VMUL;

    vec *v = malloc(sizeof(vec));
    v->data = malloc(alloc_size * sizeof(int));
    v->capacity = alloc_size;
    v->size = 0;
    return v;
}

/* 
    Safely free vector v from heap.
    return success flag: 1 = True, 0 = False.
*/
int vfree(vec *v){
    if (!vempty(v)){
        free(v->data);
        v->data = NULL;

        free(v);
        v = NULL;

        return 1;
    }
    else 
        return 0;
}

/*
    Return 0 if vector is NULL,
    return 1 if vector is valid.  
*/
int vcheck(vec *v){ return (v)? 1 : 0; }

/*
    Return the size of the vector v's values.
    In C, this should be accessed with v->size, 
    to reduce function overhead.
*/
size_t vsize(const vec *v) { return v->size; }

/*
    Return the capacity of the vector v.
    In C, this should be accessed with v->capacity, 
    to reduce function overhead.
*/
size_t vcapacity(const vec *v) { return v->capacity; }

/* 
    If vector v has no values or v ptr is NULL, return true. 
    1 = True, 0 = False. 
*/
int vempty(vec *v){ return (v == NULL || v->size <= 0) ? 1 : 0;}

int vat(vec *v, const int index){
    return v->data[index];
}

void vshrink(vec *v){
    v->data = (int*) realloc(v->data, (v->capacity/VDIV) * sizeof(int));
    v->capacity = v->capacity / VDIV;
}

void vgrow(vec *v) {
    v->data = (int*) realloc(v->data, (v->capacity * VMUL) * sizeof(int));
    v->capacity = v->capacity * VMUL;
}

/*
    Resize the vector v. 
    Double if capacity is reached.
    Half if size is 1/4 of capacity.
*/
void vresize(vec *v){
    if (v){
        if (vsize(v) == vcapacity(v)){
            vgrow(v);
        }
        else if (v->size < v->capacity / VDIV){
            vshrink(v);
        }
    }
    else {
        printf("Couldn't Resize null vector\n");
    }
}

/*
    Add a new integer n to the end of vector v->data.
    resize() check before operating on v->data.
*/
void vpush(vec *v, const int n){
    v->size = v->size + 1;
    vresize(v);
    if (v->size == 1)
        v->data[0] = n;

    v->data[v->size] = n;
}

/*
    Insert value n at index inside of vector v->data. 
    The new value and all elements after shift o the right.
*/
void vinsert(vec *v, const size_t index, const int n){
    if (!vcheck(v) || index > vcapacity(v)){ 
        fprintf(stderr, "\nVector not allocated, failing ...\n");
        exit(EXIT_FAILURE);
    }
    else{
        v->size = v->size + 1;
        vresize(v);
        memmove(v->data+index+1, v->data+index, *v->data);
        v->data[index] = n;
   }
}


/*
    Insert int n above index 0 for vector v.
*/
void vprepend(vec *v, const int n){
    if (!vcheck(v)){
        fprintf(stderr, "\nVector not allocated, failing ...\n");
        exit(EXIT_FAILURE);
    }
    else {
        v->size = v->size + 1;
        vresize(v);
        memmove(v->data+1, v->data, *v->data);
        v->data[0] = n;
    }
}
/*
    Remove last value added to vector v.
    Return the value, and resize the vector v,
    if needed.
*/
int vpop(vec *v){
    if (!vcheck(v)){
        fprintf(stderr,"\nVector not allocating...");
        exit(EXIT_FAILURE);
    }
    else{
        int n = v->data[v->size-1];
        vdel(v, n);
        vresize(v);
        return n; 
    }
}

/*
    Delete an array element at index in vector v.
*/
void vdel(vec *v, const int index){
    if (!vcheck(v)){ 
        fprintf(stderr, "\nVector not allocated, failing ...\n");
        exit(EXIT_FAILURE);
    }
    else{
        memmove(v->data+index, v->data+index+1,*v->data);
        v->size--;
        vresize(v);
   }                                                             
}

/*
    Returns first index with value n in vector v. 
    Returns -1 if not found.
*/
int vfind(vec *v, const int n){
   if (!vcheck(v)) {
        fprintf(stderr, "\nVector not allocated... \n");
        return -1;
   }

   else {
        for (size_t i = 0; i < v->size; i++) 
            if (v->data[i] == n) return i;
        
        return -1;
   }
}

void vprint(vec *v){
    for (size_t i= 0; i < v->size+1; i++) printf("%d ", v->data[i]);
    printf("\n");
}

void vprintall(vec *v){
    for (size_t i= 0; i < v->capacity; i++) printf("%d ", v->data[i]);
    printf("\n");
}
