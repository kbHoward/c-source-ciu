#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>

#define VSIZE 16
#define VMUL 2
#define VDIV 4 


typedef struct vec vec;
struct vec {
    size_t size;
    size_t capacity;
    int *data;
};

vec* vinit(const size_t sz);
int vfree(vec *v);
int vcheck(vec *v);
size_t vsize(const vec *v);
size_t vcapacity(const vec *v);
int vempty(vec *v);
int vat(vec *v, const int index);
void vgrow(vec *v);
void vshrink(vec *v);
void vresize(vec *v);
void vpush(vec *v, const int n);
void vinsert(vec *v, const size_t index, const int n);
void vprepend(vec *v, const int n);
int vpop(vec *v);
void vdel(vec *v, const int index);
int vfind (vec *v, const int n);
void vprint(vec *v);
void vprintall(vec *v);

#endif
