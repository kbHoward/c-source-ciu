#include <stdlib.h>
#include "slist.h"


/*
    Allocate a singly linked list
    without values.
*/
slist* allocate(){
    slist *l = (slist *) malloc(sizeof(slist));
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;

    return l;
}


/*
    Returns true if slist has size of zero.
    Returns false if slist has a size greater than zero.

 */
int is_empty(slist *lst){return (lst->size == 0)? 1 : 0;}


/* Return the value of node at front of lst. */
int front(const slist *lst){return lst->head->value;}


/* Return the value of node at back of lst. */
int back(const slist *lst){return lst->tail->value;}


/*
    Return size of singly linked list.
*/
size_t size(slist *lst) {return lst->size;}


/*
    Returns the value at index node.
    Returns -1 if the node is not found.
*/
int value_at(slist *lst, const int index) {
    list_node *itr = lst->head;
    int c = 0;

    while (itr){
        if (c == index) return itr->value;
        itr = itr->next; 
        c++;
    }

    return -1;
}


/*
    Take a integer value and return 
    an allocated node on the heap.

    Pointer next is NULL.
*/
list_node* create_node(const int value){
    list_node *n = (list_node *) malloc(sizeof(list_node));
    n->value = value;
    n->next = NULL;
    return n;
}


/* 
    Add node n to the front of singley linked list s. 
*/
void push_front(slist *lst, list_node *n){
    if (lst->head == NULL){
        lst->head = n;
        lst->size += 1;
    } else {
        n->next = lst->head;
        lst->head = n;
        lst->size += 1;
    }
}


/*
    Add node n to the back of list s. 
*/
void push_back(slist *lst, list_node *n) {
    if (lst->tail == NULL || n == NULL){}
    else {
        if (lst->size == 0) push_front(lst, n); 
        lst->tail->next = n;
        lst->tail = n; 
        lst->size = lst->size + 1;
    }
}


/*
    Return a pointer to the node at index.
    Return NULL if node isn't found.
*/
list_node* node_at(slist *lst, const int index){
  
    list_node *itr = lst->head;
    int c = 0;

    while(itr) {
        if (c == index) return itr;
        itr = itr->next;
        c++;
    }
    
    return NULL;
}


/*
    Safely Free the linked list node.
    Returns -1 if failed.
 */
int free_list(slist *lst){
    if (lst->head == NULL) {return 1;}
    else {
        list_node *tmp;
        while (lst->head != NULL){
            tmp = lst->head;
            lst->head = lst->head->next;
            free(tmp);
            tmp = NULL;
        }

        free(lst);
        if (lst) lst = NULL;
    }
        
    return 0;
}


/*
    Remove front item index from linked list,
    return it's value.
 */
int pop_front(slist *lst){
    if (!lst->head){return -1;}
    else {
        int v = lst->head->value;
        list_node *tmp = lst->head;
        lst->head = lst->head->next;

        free(tmp);
        if (tmp) tmp = NULL;
        lst->size = lst->size - 1;
        return v;
    }
}


/*
    Removes the end item from the linked list.
    Return the node value.

    Resetting the list tail is not done in this function.
*/
int pop_back(slist *lst){
    int v = -1;
    if (!lst->tail) {return v;}
    else {
        v = lst->tail->value;

        free(lst->tail);
        if(lst->tail){lst->tail = NULL;}
        lst->size = lst->size - 1;
/*
        lst->tail = node_at(lst, lst->size-1);
        lst->tail->next = NULL;
*/
    }
    return v;
}


/*
    Insert node n at index in lst. 
    Not appropriate for adding to,
    front or back of the list lst.

    Returns operation status.
*/
int insert(slist *lst, list_node *n, const int index) {
    int v = -1;
    if (lst == NULL || n == NULL || 
            index < 0 || index > lst->size-1) return v;
    else { 
        list_node *itr = lst->head;
        int c = 0;
        while (itr){
            if (c+1 == index) {
                list_node *tmp = itr->next;
                itr->next = n;
                n->next = tmp;
                if (tmp) tmp = NULL;
                free(tmp);
                v = 0;
                lst->size = lst->size + 1;
            }
            itr = itr->next;
            c++;
        }
        free (itr);
        if (itr) itr = NULL;
    }
    return v;
}


/*
    Remove node n at given index 
    from list lst.

    Will not operate on head or tail.

    Return operation status.
*/
int erase(slist *lst, const int index) {
    int v = -1;
    if (lst == NULL || index <= 0 ||
        index > lst->size) return v;

    else {
        list_node *itr = lst->head;
        int c = 0;
        while (itr) {
            if (c+1 == index) {
                list_node *ptr = itr->next->next;
                free(itr->next);
                itr->next = ptr;
                v = 0;
                lst->size = lst->size - 1;
            }
            itr = itr->next; 
            c++;
        }
    }
    return v;
}


/*
    Returns value at node from nth from the end of slist.
*/
int value_n_from_end(const slist *lst, const int index) {
    int v = -1, t = (lst->size - index) -1;
    if (lst == NULL || index <= 0 ||
        index > lst->size || t < 0) return v;
    else {
        int c = 0;
        list_node *itr= lst->head;
        while (itr && c != t){
            itr = itr->next;
            c++;
        }
        v = itr->value; 
    }
    return v;
}


/*
    Reverses list lst.
*/
void reverse(slist *lst){
    list_node *itr = lst->head, *next = NULL, *prev = NULL;
    while (itr != NULL){
        next = itr->next;
        itr->next = prev;
        prev = itr;
        itr = next;
    }
    lst->head = prev;

    next = NULL;
    prev = NULL;
}


/*
    Removes the first value in list lst
    with value v.
*/
void remove_value(slist *lst, const int v) {
    list_node *itr = lst->head;
    int index = 0;
    while (itr){
        if (itr->value == v) {erase(lst, index); break;}
        itr = itr->next;
        index++;
    }
}