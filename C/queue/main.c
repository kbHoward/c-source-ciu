#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "queue.h"
#include "slist.h"

int main() {

    list_queue *q = malloc(sizeof(list_queue));
    q->head = NULL;
    q->tail = NULL;
    enqueue(q, 10);
    enqueue(q, 20);
    enqueue(q, 30);
    enqueue(q, 40);

    printf("dequeue value: %d\n", dequeue(q));
    printf("dequeue value: %d\n", dequeue(q));

    list_node *itr = q->head;
    if (itr){
        while (itr->next){
            printf("%d\n", itr->value);
            itr=itr->next;
        }

        printf("%d\n", itr->value);
        printf("\nHead: %d\n", q->head->value);
        printf("\nTail: %d\n", q->tail->value);
    }
    free_queue(q);
    return 0;
}