#ifndef QUEUE_H
#define QUEUE_H

#include "../SinglyLinkedList/slist.h"

typedef struct list_queue list_queue;
struct list_queue{
    list_node *head;
    list_node *tail;
};

int enqueue(list_queue *q, const int value);
int dequeue(list_queue *q);
int empty(const list_queue *q);
int free_queue(list_queue *q);
#endif