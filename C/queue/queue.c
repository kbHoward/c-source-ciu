#include "queue.h"
#include "slist.h"
#include <stdio.h>

/*
    Add value to to buffer at tail.
*/
int enqueue(list_queue *q, const int value){
    int success = 0;
    list_node *n = create_node(value);

    if (empty(q)){
        q->head = n;
        q->tail = n;
        success = 1;
    } 
    else {
        if (q->tail){
            q->tail->next = n;
            q->tail = n;
        }
        else {
            list_node *itr = q->head;
            while (itr->next){
                itr = itr->next;
            }
            itr->next = n;
            q->tail = itr->next;
            success = 1;
            // itr = NULL;
            free(itr);
        }
    }

    return success;
}

/*
    Returns value and removes least recently used element.
*/
int dequeue(list_queue *q){
    int value = -1;
    if (!q->head){
        fprintf(stderr, "Could not dequeue NULL ptr!\n");
    } else {
        list_node *n = q->head;
        value = n->value;
        q->head = q->head->next;
        free(n);
        if (n) n = NULL;
    }
    return value;
}

/*
    Return true if queue is empty.
*/
int empty(const list_queue *q){return (q->head == NULL)? 1 : 0;}

/*
    Free the queued list, and the pointer q.
    Return the status of free;
*/
int free_queue(list_queue *q){
    int value = -1;
    list_node *tmp;
    while (q->head){
        tmp = q->head;
        q->head = q->head->next;
        free(tmp);
        tmp = NULL;
    }
    if (empty(q)) value = 1;
    free(q);
    return value;
}