#include <stdio.h>

/* 
    Return a target t in sorted array l.
    Return -1 if not found.
*/
int binary_search(const int t, const size_t sz, const int l[sz]){
    int ln = 0, rn = sz, mn = 0;
    if (l[0] == t) return 0;

    while (ln+1 < rn){
        mn = (ln + rn) / 2; // get middle index
        if (l[mn] < t) {ln = mn;}
        else if (l[mn] > t) {rn = mn;}
        if (l[mn] == t) return mn;
    }
    return -1;
}
/*
    Return index of target t in sorted array l.
    Return -1 if fail.
*/
int bin_recurse(const int t, const size_t sz, const int l[sz], size_t begin, size_t end){
    int mn = -1;
    while (begin+1 < end){
       mn = (begin + end) / 2; 
       if (l[mn] < t){return bin_recurse(t, sz, l, mn, end);}
       if (l[mn] > t){return bin_recurse(t, sz, l, begin, mn);}
       if (l[mn] == t) break;
    }
    return mn;
}

int main(){

    int arr[10000];
    for (size_t x = 0; x < 10000; x++) arr[x] = x+1;
    printf("%d\n", bin_recurse(5000, 10000, arr, 0, 10000));

    return 0;
}