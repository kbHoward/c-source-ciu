#ifndef HASH_H
#define HASH_H

/* 
    Default hashmap size. 
    Higher hashmaps sizes lessen 
    likelyhood of collisions.
*/

#define HASH_SIZE 256

typedef struct item item;
struct item {
    char *key;
    int value;
};

int hash(const char *k);
void add(char *key, const int value, item *map);
int exists(const char *key, item *map);
int get (const char *key, item *map);
void remove_index(const char *key, item *map);

#endif