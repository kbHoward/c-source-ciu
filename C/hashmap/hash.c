#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
    Hash function.
*/
int hash(const char *k) {
    int sum = 0;
    for (size_t c = 0; c < strlen(k); c++) sum += k[c];
    return sum % HASH_SIZE;
}

/* 
    Add value to hashmap.
    Alternativly, overwrite existing value if
    one already exists at the key.
*/
void add(char *key, const int value, item *map) {
    item i = {key, value};
    /* Check for occupied value at index. */
    if (map[hash(key)].key == key) {};
    map[hash(key)] = i;
}

/* 
    Uses hash function to determine if a key value exists.
*/
int exists(const char *key, item *map){
    int exists = 0;
    if (map[hash(key)].value != 0)
        exists = 1;
    return exists;
}

int get(const char *key, item *map){return map[hash(key)].value;}
void remove_index(const char *key, item *map){map[hash(key)].value = 0;}