#include <stdio.h>

#include "hash.h"

int main(){
    int map[HASH_SIZE];
    add("Hello", 6182, map);
    add("Yo", 9999, map);
    printf("%d\n", get("Hello", map));
    printf("%d\n", get("Yo", map));
    printf("%d\n", get("re", map));
    return 0;
}